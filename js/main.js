
$(document).ready(function(e){	

	/*$("#menu-mobile").css("display", "none");*/
	// $("#cabecera").on("click","#btn-estudio-gratis",function(e){
	// 	e.preventDefault();	
	// 	if ($("#estudio-gratis").is(":visible")) {
	// 		$("#estudio-gratis").hide("slow",function(){				 
	// 		});
	// 	}else{
	// 		$("#estudio-gratis").show("slow",function(){
	// 		});	 
	// 	}
	// });
	// $("#estudio-gratis").on("click","#aspa-estudio",function(e){
	// 	e.preventDefault();	
	// 	if ($("#estudio-gratis").is(":visible")) {
	// 		$("#estudio-gratis").hide("slow",function(){		 
	// 		});
	// 	}else{
	// 		$("#estudio-gratis").show("slow",function(){
	// 		});		 
	// 	}		 
	// });

	var win_height = $(window).height();
    $(".menu-movil").css("height", win_height+"px");  
	
	$(window).resize(function() {
		var re_win_height = $(window).height();
        $(".menu-movil").css("height", re_win_height+"px");	
	});

	/* menu movil */
    $("#menu-movil").css("display", "none");
	$("#cabecera").on("click","#menu-hamburguesa",function(e){
		e.preventDefault();		
		if ($("#menu-movil").is(":visible")) {
			$("#menu-movil").fadeOut("slow",function(){				 
			});
		}else{
			$("#menu-movil").fadeIn("slow",function(){
			});			 
		}
	});
	$("#menu-movil").on("click","#cerrar-menu-movil",function(e){
		e.preventDefault();	
		if ($("#menu-movil").is(":visible")) {
			$("#menu-movil").fadeOut("slow",function(){				 
			});
		}else{
			$("#menu-movil").fadeIn("slow",function(){
			});			 
		}		 
	});
	/* FIN menu movil */


	/* slide estudio */
	$("#btn-estudio-gratis").on('click', function(){
    	$('#estudio-gratis').toggleClass('muestra');
	});

	$("#aspa-estudio").on('click', function(){
    	$('#estudio-gratis').toggleClass('muestra');
	});
	/* FIN slide estudio */

	/* fomulario de contacto */
	$.validator.setDefaults({
		submitHandler: function() {
			$.ajax({
				data: $("#form-contacto").serialize(),
				url: 'ajax/ajax_envia_consulta_contacto.php',
				type: 'post',			
					success: function(){
					$("#form-contacto").html('<p class="text-align-c color-w-0100">Hemos recibido su consulta. En breve nos pondremos en contacto usted. Un saludo.</p>');
				  }
				})
		}
	});	
	$("#form-contacto").validate({
		rules: {
			nombre_contacto: {
				required: true
			},
			email_contacto: {
				required: true,
				email: true
			},
			tel_contacto: {
				required: true
			},
			direccion_contacto: {
				required: true
			},
			ciudad_contacto: {
				required: true
			},
			cp_contacto: {
				required: true
			},
			consulta_contacto: {
				required: true
			}
		},
		messages: {
			nombre_contacto: {
				required: "Rellene este campo con su nombre"
			},
			email_contacto: {
				required: "Rellene este campo con su e-mail",
				email: "Rellene este campo con un e-mail válido"
			},
			tel_contacto: {
				required: "Rellene este campo con su teléfono"
			},
			direccion_contacto: {
				required: "Rellene este campo con su dirección"
			},
			ciudad_contacto: {
				required: "Rellene este campo con su ciudad"
			},
			cp_contacto: {
				required: "Rellene este campo con su código postal"
			},
			consulta_contacto: {
				required: "Rellene este campo con el motivo de su consulta"
			}
		}
	});
	/* FIN fomulario de contacto */


	/* fomulario de estudio gratuito */
	$.validator.setDefaults({
		submitHandler: function() {
			$.ajax({
				data: $("#form-estudio").serialize(),
				url: 'ajax/ajax_envia_estudio.php',
				type: 'post',			
					success: function(){
					$("#form-estudio").html('<p class="text-align-c color-w-0100">Hemos recibido su consulta. En breve nos pondremos en contacto usted. Un saludo.</p>');
				  }
				})
		}
	});	
	$("#form-estudio").validate({
		rules: {
			nombre_contacto: {
				required: true
			},
			email_contacto: {
				required: true,
				email: true
			},
			tel_contacto: {
				required: true
			},
			ciudad_contacto: {
				required: true
			}
		},
		messages: {
			nombre_contacto: {
				required: "Rellene este campo con su nombre"
			},
			email_contacto: {
				required: "Rellene este campo con su e-mail",
				email: "Rellene este campo con un e-mail válido"
			},
			tel_contacto: {
				required: "Rellene este campo con su teléfono"
			},
			ciudad_contacto: {
				required: "Indíquenos el motivo de su consulta"
			}
		}
	});
	/* FIN fomulario de estudio gratuito */


	/* scroll animado */
	$("#cabecera").on("click","#btn-control-independiente",function(e){
		e.preventDefault();
		var desti = $("#control-independiente").offset().top;
		$("html, body").animate({scrollTop: desti}); 
	});
	$("#cabecera").on("click","#btn-termostatos",function(e){
		e.preventDefault();
		var desti = $("#termostatos").offset().top;
		$("html, body").animate({scrollTop: desti}); 
	});
	$("#cabecera").on("click","#btn-wifi",function(e){
		e.preventDefault();
		var desti = $("#wifi").offset().top;
		$("html, body").animate({scrollTop: desti}); 
	});
	$("#cabecera").on("click","#btn-consumo-energetico",function(e){
		e.preventDefault();
		var desti = $("#consumo-energetico").offset().top;
		$("html, body").animate({scrollTop: desti}); 
	});
	$("#cabecera").on("click","#btn-compacto-silicio",function(e){
		e.preventDefault();
		var desti = $("#compacto-silicio").offset().top;
		$("html, body").animate({scrollTop: desti}); 
	});
	/* FIN scroll animado */


})